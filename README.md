# xivo-install-ansible

Install xivo using ansible playbooks

# Usage

This installer installs xivo to docker swarm cluster.
Install this package on server which has access to your new xivo servers
(installer server). Installer server can be one of new xivo servers.
Run command
xivo-install.py xivo

## Prerequisities

You need 
- proper number of servers with basic debian installation.
- acces using ssh-key to each server from installer server


## Currently supported deployments are 
- xivo only on 1 server
- xivo UC on 1 or 2 servers
- xivo CC on 1 - 4 servers


## Start / Stop

On the main server use command

xivo-stack start|stop



## Installation process

xivo-install.py uses simple TUI interface to gather information about desired
deployment.

1/ Xivo parts to be installed and number of servers.
Select which xivo parts should be installed and how meny servers to use.

2/ IPs of servers.
Insert public IPs of xivo servers. Never use localhost or similar addresses
even if your installer server is one of xivo servers.

3/ Select VoIP deployment type
This is method, how to solve networkong problems of several services with
speacial network communication
- asterisk - needs to expose too many UDP for RTP
- provd - for TFTP needs to expose many UDP ports
- dhcpd - needs to receive dhcp request broadcasts

There are 3 methods how to solve these problems
- run these server on host, not in container
- use ingress anyway for asterisk and provd, expose known ranges of ports, run dhcpd on host
- ipvlan - connect containers to docker ipvlan network associated with one of host interfaces.

In this moment all 3 methods are at least partially implemented, but main
focus is on ipvlan method, which allows to have everything in containers, communicating
on overlay network and at the same time does not have a problem with ports exposion.




### ipvlan network configuration specific parameters
- voip network\
parameter --subnet of 'docker network create' command
it must be network on the parent interface of the ipvlan network
- voip dummy range\
parameter --ip-range  of 'docker network create' command
it is ip block which is used for allocation of ip addresses for containers
connected to the network. Addresses from this range asigned by docker to asterisk and provd
containers cannot be used by xivo, beacause we cannot controll which ip will be assigned
to these containers
Instead, additional addresses defined for asterisk and provd are configured on
proper interface in container during entrypoint.
- asterisk IP address\
ip address assigned to asterisk container. Must be outside of "voip dummy range" to avoid ip conflicts
- provd IP address
ip address assigned to provd container. Must be outside of "voip dummy range" to avoid ip conflicts
- GW IP\
default route in ipvlan network. If host default route is in ipvlan subnet,
this field can be left empty and address will be found automatically
- voip interface name\
parameter -o parent=  of 'docker nework create' command
Can be left empty unless you want to create new VLAN interface for the
ipvlan network. In such case interface name must be standtard vlan interface
name (like ens192.200 for parent interface ens192 and vlan 200)
- dhcp range start\
first ip of address range available for dhcp
- dhcp range end\
last ip of address range available for dhcp
- DHCP router\
option 'routers' for DHCP server

### On host voip deployment specific parameters
- EXTENNAL_ASTERISK_HOST address
IP address which will be used to contact asterisk which runs on host or is
  external

### Ingress voip deployment parameters
- asterisk RTP port range start
- asterisk RTP port range end

Note 1: This deployment method does not solve port range for tftp service inn
provd. In current provd port range for tftp transfer cannot be configured

Note 2: Exposing large range of ports takes wery long time, because docker
creates firewall rules one by one for every port from range.
It is also posible (and it was tested too) to make simple server on the host
which listens on docker socker for service evenst and when asterisk starts,
it can create ingress rules in firewall for whole port range instead for
one rule every port. In that case asterisk RTP ports and tftp transfer port
do not need to be exposed by docker.

## MDS

MDS installer 
- can install one or more MDS servers at once.
- expects ipvlan voip deployment type is used in xivo installation
- requires running xivo stack.

To install MDS use command
xivo-install.py mds

Simple TUI asks for number of MDS to install 

For each MDS server TUI will ask for
- MDS name
- MDS descrioption
- MDS IP address
- MDS asterisk IP in ipvlan

At the and TUI asks for xivo main server IP address and starts the
installation


## Start / stop

On the MDS server use command
xivo-mds start|stop


   










