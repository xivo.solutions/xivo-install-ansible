#!/usr/bin/env python3

import sys
import subprocess
import ipaddress
import json
import shlex
import re

PLAYBOOKS_DIR="/usr/share/xivo/ansible/playbooks"
XIVO_INSTALL_PLAYBOOK=PLAYBOOKS_DIR + "/xivo-install.yml"
MDS_INSTALL_PLAYBOOK=PLAYBOOKS_DIR + "/mds-install.yml"

class XivoModule:
    def __init__(self, name, label, descr):
        self.name = name
        self.label = label
        self.descr = descr

class ServerType:
    def __init__(self, modules):
        self.modules = modules

    def descr(self):
        return "+".join([ m.descr for m in self.modules ])

    def get_dict(self):
        return {
            'modules': [ m.name for m in self.modules ],
            'labels': [ m.label for m in self.modules ]
        }
    

class DeploymentType:
    def __init__(self, servers):
        for s in servers:
            if len(s) != 2:
                raise TypeError()
            
            if not isinstance(s[0], ServerType):
                raise TypeError()
        
        self.servers = servers
        
        
    def descr(self):
        cnt = self.srv_count()
        srv = "{} server{}, ".format(cnt, "" if cnt == 1 else "s")
        return srv + " / ".join([ s[0].descr() for s in self.servers ])

    def srv_count(self):
        return len(self.servers)

    def get_dict(self):
        servers = [ s[0].get_dict() for s in self.servers ]
        for i in range(self.srv_count()):
            servers[i]['manager'] = self.servers[i][1]
        
        return servers
        
        

class Deployment:
    def __init__(self, depl_type):
        
        self.depl_type = depl_type
        self.ips = [None] * self.srv_count()
    
    def srv_count(self):
        return self.depl_type.srv_count()
    
    def set_ip(self, srv_index, ip):
        if srv_index < 0 or srv_index >= self.srv_count():
            raise Error
        
        self.ips[srv_index] = ip
        
        
    def get_dict(self, exclude_main = False):
        servers = self.depl_type.get_dict()
        for i in range(self.srv_count()):
            servers[i]['ip'] = self.ips[i]
        if exclude_main:
            servers.pop(0)    
        return { 'servers': servers }

        
class Whiptail:
    
    path = "whiptail"
    
    def __init__(self, title, init = ''):
        self.title = title
        self.init = init
    
    def set_path(self, path):
        self.path = path
    
    def run(self, args, cancelcheck = None):
        cmd = [self.path] + ["--title", self.title] + args
        res = subprocess.run(cmd, stderr=subprocess.PIPE)
        
        # Cancel or No button pressed - ret == 1
        # Esc key pressed - ret == 255 and no output on stderr
        # whiptaill error - ret == 255 and usage printed to stderr 

        if res.returncode == 1 or res.returncode == 255 and res.stderr == b'':
            if cancelcheck:
                cancelcheck()
            return None
        elif res.returncode != 0:
            raise RuntimeError("Whiptail error")
        elif res.stderr == b'':
            return ['']
        else:
            return res.stderr.decode('ascii').split("\n")
        

    def run_with_validator(self, args = [], cancelcheck = None, validator = None, show_error = None):
        while True:
            value = self.run(args, cancelcheck);
            if validator is None:
                return value.strip();
            try:
                return validator.validate(value)
            except ValueError as err:
                if show_error is None:
                    print(str(err), file=sys.stderr)
                else:
                    show_error(str(err))
                
            self.init = value
    

class WhiptailRadiolist(Whiptail):
    
    def __init__(self, title, items, text = "", width = 60, init = ''):
        super().__init__(title, init)
        self.text = text
        self.width = width
        self.items = items
        
        
    def run(self, args=[], cancelcheck = None):
        listh = len(self.items)
        h = listh + 5
        
        fullargs = args + ["--radiolist", self.text, str(h), str(self.width), str(listh) ]
        for i in self.items:
            fullargs += [ i[0], i[1], "off" ]
        
        ret =  super().run(fullargs, cancelcheck)
        return None if ret is None else ret[0]


class WhiptailInputbox(Whiptail):
    
    def __init__(self, title, text = "", height = 10, width = 60, init = ""):
        super().__init__(title, init)
        self.text = text
        self.height = height
        self.width = width
        
        
    def run(self, args=[], cancelcheck = None):
        
        fullargs = args + [
            "--inputbox", 
            self.text, 
            str(self.height),
            str(self.width),
            self.init
        ]
        
        ret = super().run(fullargs, cancelcheck)
        return None if ret is None else ret[0]

class WhiptailMsgbox(Whiptail):
    
    def __init__(self, title, msg, height = 7, width = 60):
        self.msg = msg
        self.height = height
        self.width = width
        
        super().__init__(title, '')

    def run(self, args=[]):
        fullargs = args + ["--msgbox", self.msg, str(self.height), str(self.width)]
        super().run(fullargs)


class WhiptailYesno(Whiptail):
    
    def __init__(self, title, msg, height = 7, width = 60):
        self.msg = msg
        self.height = height
        self.width = width
        
        super().__init__(title)

    def run(self, args=[]):
        fullargs = args + ["--yesno", self.msg, str(self.height), str(self.width)]
        res = super().run(fullargs)
        
        return False if res is None else True
        


class Validator:
    def __init__(self, args = {}):
        self.args = args
    
    def validate(self, value):
        raise NotImplementedError


class StringValidator(Validator):
    ARG_NOSTRIP = 'nostrip'
    ARG_ALLOW_EMPTY = 'allow_empty'
    ARG_RETURN_ORIG = 'return_orig'

    @property
    def arg_nostrip(self):
        return self.args.get(self.ARG_NOSTRIP, False)
    
    @property
    def arg_allow_empty(self):
        return self.args.get(self.ARG_ALLOW_EMPTY, False)
    
    @property
    def arg_return_orig(self):
        return self.args.get(self.ARG_RETURN_ORIG, False)
    
    def real_validator(self, value):
        return value

    def validate(self, value):
        if not self.arg_nostrip:
            value = value.strip()
    
        if value == '':
            if self.arg_allow_empty:
                converted = None
            else:
                raise ValueError("Empty value not allowed")
        else:        
            try:
                converted = self.real_validator(value)
            except ValueError as err:
                raise err
        
        if self.arg_return_orig:
            return value
        else:
            return converted

    
class IPv4AddressValidator(StringValidator):
    ARG_HOST_IN_NET = 'host_in_net'

    def __init__(self, args):
        super().__init__(args)
        try:
            self.args[self.ARG_HOST_IN_NET] = ipaddress.IPv4Network(self.args[self.ARG_HOST_IN_NET])
        except KeyError:
            pass
    
    @property
    def arg_host_in_net(self):
        return self.args.get(self.ARG_HOST_IN_NET)

    def real_validator(self, value):
        try: 
            res = ipaddress.IPv4Address(value)
        except ipaddress.AddressValueError as err:
            raise ValueError(str(err))

        if self.arg_host_in_net is not None:
            if not res in self.arg_host_in_net:
                raise ValueError ("IP must be host address in network {}".format(str(self.arg_host_in_net)))
        
            if res == self.arg_host_in_net[0]:
                raise ValueError ("IP is network address in network {}".format(str(self.arg_host_in_net)))
        
            if res == self.arg_host_in_net[-1]:
                raise ValueError ("IP is broadcast address in network {}".format(str(self.arg_host_in_net)))

        return res
    
class IPv4NetworkValidator(StringValidator):
    ARG_MAX_PREFIXLEN = 'max_prefixlen'
    ARG_STRICT = 'strict'
    ARG_IN_NET = 'in_net'

    def __init__(self, args):
        super().__init__(args)
        try:
            self.args[self.ARG_MAX_PREFIXLEN] = int(self.args[self.ARG_MAX_PREFIXLEN])
            if self.arg_max_prefixlen > 32:
                raise ValueError("Validator arg 'max_prefixlen' must be <= 32")
        except KeyError:
            pass

        try:
            self.args[self.ARG_STRICT] = bool(self.args[self.ARG_STRICT])
        except KeyError:
            pass

        try:
            self.args[self.ARG_IN_NET] = ipaddress.IPv4Network(self.args[self.ARG_IN_NET])
        except KeyError:
            pass
        
    @property
    def arg_max_prefixlen(self):
        return self.args.get(self.ARG_MAX_PREFIXLEN, 32)
    
    @property
    def arg_strict(self):
        return self.args.get(self.ARG_STRICT, True)

    @property
    def arg_in_net(self):
        return self.args.get(self.ARG_IN_NET)

    def real_validator(self, value):
        try: 
            res = ipaddress.IPv4Network(value, strict = self.arg_strict)
        except ValueError as err:
            raise err
        
        if res.prefixlen > self.arg_max_prefixlen:
            raise ValueError("Network is too small, needed at least /{}".format(self.arg_max_prefixlen))
        
        if self.arg_in_net is not None and not res.subnet_of(self.arg_in_net):
            raise ValueError("Network must be subnet of {}".format(str(self.arg_in_net)))
        
        return res

class IPv4InterfaceValidator(StringValidator):
    def real_validator(self, value):
        try:
            res = ipaddress.IPv4Interface(value)
        except ValueError as err:
            raise err
        
        return res

class IntegerValidator(StringValidator):
    ARG_MIN = 'min'
    ARG_MAX = 'max'
    ARG_VALUE_NAME = 'value_name'

    def __init__(self, args):
        super().__init__(args)
        try:
            self.args['min'] = int(self.args['min'])
        except KeyError:
            pass
        
        try:
            self.args['max'] = int(self.args['max'])
        except KeyError:
            pass
    
    @property
    def arg_min(self):
        return self.args.get(self.ARG_MIN, ~sys.maxsize)
        
    @property
    def arg_max(self):
        return self.args.get(self.ARG_MAX, sys.maxsize)
    
    @property
    def arg_value_name(self):
        return self.args.get(self.ARG_VALUE_NAME, 'Value')
    
        
    def real_validator(self, value):
        try:
            intval = int(value)
        except:
            raise ValueError("{} must be integer".format(self.arg_value_name))
    
        if intval < self.arg_min:
            raise ValueError("{} must be >= {}".format(self.arg_value_name, self.arg_min))

        if intval > self.arg_max:
            raise ValueError("{} must be <= {}".format(self.arg_value_name, self.arg_max))
        
        return intval


class InterfaceNameValidator(StringValidator):
    # TODO exclude lo, vethXXXX, docker*, speacial chars
    def real_validator(self, value):
        return value


class RegexValidator(StringValidator):
    ARG_REGEX = 'regex'
    
    def __init__(self, args):
        super().__init__(args)
        try:
            self.args[self.ARG_REGEX] = re.compile(self.args[self.ARG_REGEX])
        except Exception as e:
            raise e
    
    @property
    def arg_regex(self):
        return self.args['regex']
    
    def real_validator(self, value):
        if self.args['regex'].search(value) is None:
            raise ValueError("{} must match regex {}".format(self.arg_value_name, self_arg_regex.pattern))
        
        return value 


module_xivo = XivoModule("xivo", "xivo_main", "Xivo")
module_xuc = XivoModule("xuc", "xivo_xuc", "XuC")        
module_rep = XivoModule("rep", "xivo_rep", "Reporting")
module_rec = XivoModule("rec", "xivo_rec", "Recording")
module_log = XivoModule("log", "xivo_log", "Logging")

server_type_xivo = ServerType([module_xivo, module_log])
server_type_xivo_xuc = ServerType([module_xivo, module_xuc, module_log])

server_type_xcc_all = ServerType([module_xivo, module_xuc, module_rep, module_rec, module_log])
server_type_xuc = ServerType([module_xuc])
server_type_xcc_xuc_rep = ServerType([module_xuc, module_rep])
server_type_xcc_xuc_rep_rec = ServerType([module_xuc, module_rep, module_rec])
server_type_xcc_rep = ServerType([module_rep])
server_type_xcc_rec = ServerType([module_rec])

depl_types = {
    "xivo_only": DeploymentType([(server_type_xivo, True)]),
    "xivouc1": DeploymentType([(server_type_xivo_xuc, True)]),
    "xivouc2": DeploymentType([(server_type_xivo, True), (server_type_xuc, False)]),
    "xivocc1": DeploymentType([(server_type_xcc_all, True)]),
    "xivocc2": DeploymentType([(server_type_xivo, True),(server_type_xcc_xuc_rep_rec, False)]),
    "xivocc3": DeploymentType([(server_type_xivo, True),(server_type_xcc_xuc_rep, True), (server_type_xcc_rec, True)]),
    "xivocc4": DeploymentType([(server_type_xivo, True),(server_type_xuc, True), (server_type_xcc_rep, True), (server_type_xcc_rec, False)]) 
}

def proc_cancel():
    w = WhiptailYesno("Cancel", "Do you want to cancel installation?")
    res = w.run()
    
    if res:
        exit(0)
    

def show_msg(title, msg):
    w = WhiptailMsgbox(title, msg)
    w.run()


def show_error(msg):
    show_msg("Error", msg)


#################################
#
# UI dialog functions
#    
#################################

def select_voip_deployment():
    items = [
        ("extern", "Asterisk on host or external"),
        ("ingress", "Asterisk on stack, ingress nework"),
        ("ipvlan", "Asterisk on stack, ipvlan network"),
    ]
    w = WhiptailRadiolist("VOIP deployment", items, width=90)
    
    res = ""
    while True:
        res = w.run(["--notags"], proc_cancel)
        if res is None:
            continue
        
        if res == '':
            show_msg("Error", "Nothing selected")
            continue
            
        return res

def get_ip_interface(title, txt, value, validator_args = {}):
    validator = IPv4InterfaceValidator()

    w = WhiptailInputbox(title, txt, 10, len(txt) + 10, value)
    ipif = w.run_with_validator(cancelcheck = proc_cancel, validator = validator, show_error = show_error)

    return ipif

def get_ip_address(title, txt, value, validator_args = {}):
    validator = IPv4AddressValidator(validator_args);
    
    w = WhiptailInputbox(title, txt, 10, len(txt) + 10, value)
    ip = w.run_with_validator(cancelcheck = proc_cancel, validator = validator, show_error = show_error)

    return ip

def get_ip_network(title, txt, value, validator_args = {}):
    validator = IPv4NetworkValidator(validator_args)

    w = WhiptailInputbox(title, txt, 10, len(txt) + 10, value)
    net = w.run_with_validator(cancelcheck = proc_cancel, validator = validator, show_error = show_error)
    return net

def get_port(title, txt, value, validator_args = {}):
    validator_args['min'] = 10000
    validator_args['max'] = 65535
    validator = IntegerValidator(validator_args)

    w = WhiptailInputbox(title, txt, 10, len(txt) + 10, value)
    port = w.run_with_validator(cancelcheck = proc_cancel, validator = validator, show_error = show_error)
    
    return port

def get_interface_name(title, txt, value, validator_args = {}):
    validator = InterfaceNameValidator(validator_args)

    w = WhiptailInputbox(title, txt, 10, len(txt) + 10, value)
    ifname = w.run_with_validator(cancelcheck = proc_cancel, validator = validator, show_error = show_error)
    
    return ifname


def get_voip_config():
    voip_depl = select_voip_deployment()
    if voip_depl == "extern":
        voip_options = config_voip_extern()
    elif voip_depl == "ingress":
        voip_options = config_voip_ingress()
    elif voip_depl == "ipvlan":
        voip_options = config_voip_ipvlan()
    else:
        raise RuntimeError("Unknown viop deployment type '{}'".format(voip_depl))
    
    return { "voip_deployment_type": voip_depl, "voip_options": voip_options }
    

def select_deployment_type():
    items = [ (k, v.descr()) for k, v in depl_types.items() ]
    w = WhiptailRadiolist("Select deployment type", items, width=90)
    
    res = ""
    while True:
        res = w.run(["--notags"], proc_cancel)
        if res is None:
            continue
        
        if res == '':
            show_msg("Error", "Nothing selected")
            continue
            
        return res


def get_server_ip(server_type, used_ips, ip = ""):
    title = "Server IP"
    txt = "IP address of server with {}".format(server_type.descr())
    input = ''

    while True:

        w = WhiptailInputbox(title, txt, 10, 60, input)
        ip = get_ip_address(title, txt, '')

        if ip is None:
            input = ''
            continue


        if ip in used_ips:
            show_error("IP already used on other server")
            continue

        return ip         

def get_server_ips(depl):
    used = []
    i = 0
    for s in depl.depl_type.servers:
        ip = get_server_ip(s[0], used)
        if ip is None:
            return None
        used.append(ip)
        depl.set_ip(i, format(ip))
        i = i + 1

def get_inventory_str(depl, exclude_main=False):
    d = depl.get_dict(exclude_main)
    ips = [ s['ip'] for s in d['servers'] ]
    return ','.join(ips) + ','

def config_voip_extern():
    ip = get_ip_address("Asterisk address", "ASTERISK_EXTERN_HOST ip", depl.ips[0]);
    return { "asterisk_extern_ip": format(ip) }

def config_voip_ingress():
    start = get_port("", "Asterisk RTP port range start", "12000")
    end = get_port("", "Asterisk RTP port range end", "13000")
    
    return  { "rtp_port_range_start": start, "rtp_port_range_end": end }
    
    
def config_voip_ipvlan():
    net = get_ip_network("Voip network", "Voip network net/bitmask", '',
        validator_args = { IPv4NetworkValidator.ARG_MAX_PREFIXLEN: 29 })

    dummy_range = get_ip_network("Voip dummy range", "/30 block of unused IPs in voip net", '',
        validator_args = {
            IPv4NetworkValidator.ARG_MAX_PREFIXLEN: 30,
            IPv4NetworkValidator.ARG_IN_NET: net
        }
    )

    ast_ip = get_ip_address("Asterisk IP", "Asterisk IP address", '', 
        validator_args = { IPv4AddressValidator.ARG_HOST_IN_NET: net })

    provd_ip = get_ip_address("Provd IP", "Provd IP address", '',
        validator_args = { IPv4AddressValidator.ARG_HOST_IN_NET: net })
    
    gw_ip = get_ip_address("GW IP", "VoIP net GW address, optional", '',
        validator_args = { IPv4AddressValidator.ARG_HOST_IN_NET: net, StringValidator.ARG_ALLOW_EMPTY: True })

    ifname = get_interface_name("Voip interface", "Voip interface name,\n leave empty if voip network already exists", '', 
        validator_args = { StringValidator.ARG_ALLOW_EMPTY: True })
    
    dhcp_range_start = get_ip_address("DHCP range start", "DHCP range start", '',
        validator_args = { IPv4AddressValidator.ARG_HOST_IN_NET: net })
    
    dhcp_range_end = get_ip_address("DHCP range end", "DHCP range end", '',
        validator_args = { IPv4AddressValidator.ARG_HOST_IN_NET: net })
    
    dhcp_router = get_ip_address("DHCP router", "DHCP option routers", '',
        validator_args = { IPv4AddressValidator.ARG_HOST_IN_NET: net, StringValidator.ARG_ALLOW_EMPTY: True })
    
    ret = { 
        "interface": ifname,
        "network": format(net), 
        "dummy_ip_range": format(dummy_range),
        "asterisk_ip": format(ast_ip),
        "provd_ip": format(provd_ip),
        "gateway": '' if gw_ip is None else format(gw_ip),
        "dhcp_range_start": format(dhcp_range_start),
        "dhcp_range_end": format(dhcp_range_end),
        "dhcp_router": '' if dhcp_router is None else format(dhcp_router)
    }
    
    return ret


def install_xivo():
    t = select_deployment_type()
    depl = Deployment(depl_types[t])
    get_server_ips(depl)
    voip_config = get_voip_config()
    
    
    servers_str = json.dumps(depl.get_dict())
    inventory = get_inventory_str(depl)
    config = json.dumps(voip_config)

    cmd = ['ansible-playbook', '-i', inventory, '-e', servers_str, '-e', config, '-e', 'ansible_python_interpreter=python3', XIVO_INSTALL_PLAYBOOK ]
    with open("/tmp/xivo-ansible-installer-cmd.sh", "w") as f:
        print(shlex.join(cmd), file=f)        
    subprocess.run(cmd)

def get_mds_number(value=''):
    validator_args = {}
    validator_args['min'] = 1
    validator_args['max'] = 10
    validator = IntegerValidator(validator_args)
    
    title = "# of MDSs"
    txt = "Number of installed MDSs"

    w = WhiptailInputbox(title, txt, 10, len(txt) + 10, value)
    num = w.run_with_validator(cancelcheck = proc_cancel, validator = validator, show_error = show_error)
    
    return num


def get_mds_name(value=''):
    validator_args = { RegexValidator.ARG_REGEX: "^[\w\d_]+$" }
    validator = RegexValidator(validator_args)
    
    title = "MDS name"
    txt = "MDS name"
    
    w = WhiptailInputbox(title, txt, 10, len(txt) + 20, value)
    name = w.run_with_validator(cancelcheck = proc_cancel, validator = validator, show_error = show_error)
    
    return name

def get_mds_descr(value=''):
    title = "MDS description"
    txt = "MDS description"
    
    w = WhiptailInputbox(title, txt, 10, len(txt) + 10, value)
    descr = w.run_with_validator(cancelcheck = proc_cancel, validator = StringValidator(), show_error = show_error)
    
    return descr

def get_mds_config():
    mds = {}
    mds['name'] = get_mds_name()
    mds['description'] = get_mds_descr()
    mds['ip'] = format(get_ip_address("MDS IP", "MDS host IP address", ''))
    mds['asterisk_voip_ip'] = format(get_ip_address("MDS asterisk IP", 'MDS asterisk IP', ''))
    
    return mds

def get_mds_voip_config():
    config = {}
    
    net = get_ip_network("MDS dummy network", "Can be empty", '', {StringValidator.ARG_ALLOW_EMPTY: True} )
    config['dummy_ip_range'] = '' if net is None else net
    
    return config
    

def get_mds_inventory_str(main_server_ip, mds_list):
    ips = [ format(mds['ip']) for mds in mds_list ]
    ips.insert(0, format(main_server_ip))
        
    return ",".join(ips) + ","


def install_mds():
    num = get_mds_number();
    mds_list = []
    for i in range(num):
        mds = get_mds_config()
        mds_list.append(mds)
    main_server_ip = format(get_ip_address("Main server IP", "Xivo main server IP", ''))
    voip_config = get_mds_voip_config()

    inventory = get_mds_inventory_str(main_server_ip, mds_list)
    config = json.dumps({ "main_server": main_server_ip, "mds_servers": mds_list, "voip_config": voip_config })
    
    cmd = ['ansible-playbook', '-i', inventory, '-e', config, '-e', 'ansible_python_interpreter=python3', MDS_INSTALL_PLAYBOOK ]
    with open("/tmp/mds-ansible-installer-cmd.sh", "w") as f:
        print(shlex.join(cmd), file=f)        
    subprocess.run(cmd)
    



def usage():
    print("Usage:")
    print(sys.argv[0] + " xivo|mds")


if len(sys.argv) != 2:
    usage()
    exit(1)
elif sys.argv[1] == 'xivo':
    install_xivo()    
elif sys.argv[1] == 'mds':
    install_mds()
else:
    usage()
    exit(1)
